/**
 * A component that maps data to x and y coordinates in a container
 * with width and height.
 */
c3.plottable = function() {
    return c3.component('plottable')
        .extend(c3.withData())
        .extend(c3.withDimensions())
        .extend(c3.clippable())
        .extend({
            x: function() {
                var xScale = this.xScale();
                var xAccessor = this.xAccessor();
                return function(d, i) {
                    return xScale(xAccessor(d, i));
                };
            },
            y: function() {
                var yScale = this.yScale();
                var yAccessor = this.yAccessor();
                return function(d, i) {
                    return yScale(yAccessor(d, i));
                };
            },
            xAccessor: c3.inherit('xAccessor', c3.defaults.x),
            yAccessor: c3.inherit('yAccessor', c3.defaults.y),
            xScaleConstructor: c3.inherit('xScaleConstructor', d3.scale.linear),
            yScaleConstructor: c3.inherit('yScaleConstructor', d3.scale.linear),
            xDomain: c3.inherit('xDomain').onDefault(function() {
                if (c3.isEmpty(this.data())) return;
                
                var min = c3.checkIsNumber(d3.min(this.data(), this.xAccessor()));
                var max = c3.checkIsNumber(d3.max(this.data(), this.xAccessor()));
                return [min, max];
            }),
            yDomain: c3.inherit('yDomain').onDefault(function() {
                if (c3.isEmpty(this.data())) return;
                
                var min = c3.checkIsNumber(d3.min(this.data(), this.yAccessor()));
                var max = c3.checkIsNumber(d3.max(this.data(), this.yAccessor()));
                return [min, max];
            }),
            xRange: c3.inherit('xRange').onDefault(function() {
                var width = c3.checkIsNumber(this.width());
                return [0, width];
            }),
            yRange: c3.inherit('yRange').onDefault(function() {
                var height = c3.checkIsNumber(this.height());
                return this.cartesian() ? [height, 0] : [0, height];
            }),
            xScale: function() {
                return this.xScaleConstructor()()
                    .domain(this.xDomain())
                    .range(this.xRange());
            },
            yScale: function() {
                return this.yScaleConstructor()()
                    .domain(this.yDomain())
                    .range(this.yRange());
            },
            cartesian: c3.prop(true)
        });
};
