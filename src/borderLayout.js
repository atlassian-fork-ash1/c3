/**
 * A layout that consists of 5 regions (center, north, south, west, east),
 * each of which can contain a single component.
 *   - North and south must have their height set
 *   - West and east must have their width set
 *   
 * The width and height of center is automatically calculated based on the other regions.
 */
c3.borderLayout = function() {
    var region = function() {
        return c3.prop().set(function(component) {
            return component.parent(this);
        });
    };
    var regionsDrawable = c3.drawable()
        .extend({
            data: function() {
                return borderLayout.occupiedRegions();
            }
        }).dataKey(function(d) {
            return d.name;
        })
        .update(function(event) {
            var positions = borderLayout.positions();
            event.selection.each(function(d) {
                var regionContainer = d3.select(this);
                var position = positions[d.name];
                regionContainer.attr('transform', 'translate(' + position.left + ',' + position.top + ')');
                d.component
                    .width(position.width)
                    .height(position.height);
                d.component(regionContainer);
            });
        });
    var borderLayout = c3.component('borderLayout')
        .extend(c3.plottable())
        .extend(function() {
            regionsDrawable(this.selection());
        })
        .extend({
            center: region(),
            north:  region(),
            south:  region(),
            west:   region(),
            east:   region(),

            xRange: function() {
                return [0, this.centerWidth()];
            },
            yRange: function() {
                return [this.centerHeight(), 0];
            },
            centerWidth: function() {
                // TODO Optimise
                return this.positions().center.width;
            },
            centerHeight: function() {
                // TODO Optimise
                return this.positions().center.height;
            },
            occupiedRegions: function() {
                var borderLayout = this;
                return _.compact(_.map(['north', 'south', 'west', 'east', 'center'], function(name) {
                    var component = borderLayout[name]();
                    return component ? {
                        name: name,
                        component: borderLayout[name]()
                    } : null;
                }));
            },
            positions: function() {
                var margins = {
                    top:    c3.checkIsNumber(this.north() ? this.north().height() : 0),
                    bottom: c3.checkIsNumber(this.south() ? this.south().height() : 0),
                    left:   c3.checkIsNumber(this.west() ? this.west().width() : 0),
                    right:  c3.checkIsNumber(this.east() ? this.east().width() : 0)
                };
                var layoutWidth = c3.checkIsNumber(this.width());
                var layoutHeight = c3.checkIsNumber(this.height());

                var positions = {};
                var center = positions.center = {
                    left: margins.left,
                    top: margins.top,
                    width: layoutWidth - margins.left - margins.right,
                    height: layoutHeight - margins.top - margins.bottom
                };
                if (this.north()) {
                    positions.north = {
                        top: margins.top,
                        left: margins.left,
                        height: margins.top,
                        width: center.width
                    };
                }
                if (this.south()) {
                    positions.south = {
                        top: layoutHeight - margins.bottom,
                        left: margins.left,
                        height: margins.bottom,
                        width: center.width
                    };
                }
                if (this.west()) {
                    positions.west = {
                        top: margins.top,
                        left: margins.left,
                        height: center.height,
                        width: margins.left
                    };
                }
                if (this.east()) {
                    positions.east = {
                        top: margins.top,
                        left: layoutWidth - margins.right,
                        height: center.height,
                        width: margins.right
                    };
                }
                return positions;
            }
        });
    return borderLayout;
};
