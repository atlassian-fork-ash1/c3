c3.axis = function() {
    return c3.component('axis')
        .extend(c3.plottable())
        .extend(function() {
            this.selection().call(this.axis());
        })
        .extend({
            axisConstructor: c3.inherit('axisConstructor', d3.svg.axis),
            orient: c3.prop('bottom'),
            horizontal: function() {
                var orient = this.orient();
                return orient === 'bottom' || orient === 'top';
            },
            axis: function() {
                var horizontal = this.horizontal();
                return this.axisConstructor().call(this)
                    .scale(horizontal ? this.xScale() : this.yScale())
                    .orient(this.orient());
            }
        });
};
