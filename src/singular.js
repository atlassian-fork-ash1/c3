c3.singular = function() {
    return c3.component('singular')
        .extend(c3.withElements())
        .extend({
            enter: c3.event(),
            update: c3.event()
        })
        .extend(function() {
            var element = this.elements();
            if (!element.node()) {
                element = this.selection().append(this.elementTag());
                if (this.elementClass()) {
                    element.classed(this.elementClass(), true);
                }
                this.enter({ selection: element });
            }
            this.update({ selection: element });
        });
};
