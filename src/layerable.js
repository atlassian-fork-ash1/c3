/**
 * Allows components to be layered on top of each other and
 * share common data and/or scales
 */
c3.layerable = function() {
    var layerDrawable = c3.drawable()
        .elementClass('layer')
        .update(function(event) {
            event.selection.each(function(d, i) {
                d.component(d3.select(this).classed(d.name, true));
            });
        });

    return c3.component('layerable')
        .extend(function() {
            layerDrawable(this.selection());
        })
        .extend(c3.plottable())
        .extend({
            layers: function() {
                return layerDrawable.data();
            },
            getLayer: function(name) {
                var layer = _.findWhere(this.layers(), { name: name });
                return layer ? layer.component : undefined;
            },
            addLayer: function(name, component) {
                if (!this.getLayer(name)) {
                    this.layers().push({
                        name: name,
                        component: component.parent(this)
                    });
                }
                return this;
            },
            removeLayer: function(name) {
                var layers = this.layers();
                var removed = _.reject(layers, function(layer) {
                    return layer.name === name;
                });
                if (removed.length !== layers.length) {
                    this.layers(removed);
                }
                return this;
            }
        });
};
