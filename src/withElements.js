c3.withElements = function() {
    return c3.component('withElements')
        .extend({
            elementTag: c3.prop('g'),
            elementClass: c3.prop(''),
            elementSelector: function() {
                return this.elementClass() ?
                    this.elementTag() + '.' + this.elementClass() :
                    this.elementTag();
            },
            elements: function() {
                var containerNode = this.selection().node();
                return this.selection().selectAll(this.elementSelector()).filter(function() {
                    return this.parentNode === containerNode;
                });
            }
        });
};
