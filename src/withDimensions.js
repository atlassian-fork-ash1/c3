c3.withDimensions = function() {
    return c3.component('withDimensions')
        .extend({
            width: c3.inherit('width').onDefault(function() {
                return parseInt(this.selection().style('width'), 10) || 0;
            }),
            height: c3.inherit('height').onDefault(function() {
                return parseInt(this.selection().style('height'), 10) || 0;
            })
        });
};
