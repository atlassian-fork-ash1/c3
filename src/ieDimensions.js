/**
 * IE8 and below use r2d3 to render c3 charts. Because we need responsive
 * charts and can't set fixed dimensions on the SVG element, r2d3 fails to get
 * a useable height and width for RaphaelJS to initialise a canvas. So we look
 * for the dimensions of the parent DOM node and pass those instead.
 */
c3.ieDimensions = function () {
    // Only apply for IE8 and below
    if (document.documentMode && document.documentMode < 9) {
        return c3.component('ieDimensions')
            .extend(c3.fitToParent());
    }
};
