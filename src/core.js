var c3 = {};

/**
 * A set of common defaults that c3 components can use.
 * Components should always be written to use sensible defaults in their properties,
 * instead of null or undefined, where possible.
 * This object is NOT designed to be overwritten at runtime to change global defaults.
 */
c3.defaults = {
    x: function(d) { return d[0]; },
    y: function(d) { return d[1]; }
};

/**
 * Throws an error if the value is not a number (either NaN or other types).
 * 
 * Detect problems early by checking that values are numbers when expected,
 * rather than continuing to operate on NaNs as if things are all good.
 * 
 * @param  {*} value - value to be tested
 * @return {*} the same value that was passed in
 */
c3.checkIsNumber = function(value) {
    if (typeof value !== 'number') {
        throw new Error("Expected a number, but received: " + value);
    }
    return value;
};

c3.isEmpty = function(value) {
    return !value || !value.length;
};

/**
 * Creates a property getter/setter function a la d3's getters and setters.
 * The actual property value is stored inside a closure.
 * 
 *    var color = c3.prop('red');
 *    color(); // red
 *    color('blue'); // set value to blue
 *
 * The property's getter or setter can be customised by calling .get() or .set()
 * on the property function.
 * The getter function is passed the stored value and should return a transformed value.
 * The setter function is passed the newValue and the oldValue and should return
 * the value to be stored.
 * 
 *    c3.prop(10).get(function(value) {
 *        return value + 'px';
 *    }).set(function(newValue, oldValue) {
 *        // Prevent the value from being set below 10
 *        if (newValue < 10) return oldValue;
 *        return newValue;
 *    });
 *
 * @param {*} [defaultValue] - the initial value of the property
 */
c3.prop = function(defaultValue) {
    var value = defaultValue;

    var getter = function(value) {
        return value;
    };

    var setter = function(newValue, oldValue) {
        return newValue;
    };

    function prop(newValue) {
        if (arguments.length === 0) {
            return getter.call(this, value);
        }

        value = setter.call(this, newValue, value);

        return this;
    }

    prop.get = function(newGetter) {
        getter = newGetter;
        return this;
    };

    prop.set = function(newSetter) {
        setter = newSetter;
        return this;
    };

    return prop;
};

/**
 * Generates a c3.prop that inherits its value from parent components if
 * the prop contains a null or undefined value.
 * 
 * A defaultValue should be supplied in case no parent supplies a value.
 *
 *    c3.inherit('color', 'red');
 *
 * If the defaultValue needs to be evaluated, pass a function to .onDefault() instead.
 *
 *    c3.inherit('color').onDefault(function() {
 *        // Random color between red and blue
 *        return d3.scale.linear().range(['red', 'blue'])(Math.random());
 *    });
 * 
 * @param {string} from - the name of the property on the parent to inherit from
 * @param {*} [defaultValue] - default value that is returned if no parent returns a value
 */
c3.inherit = function(from, defaultValue) {
    var onDefault;
    var prop = c3.prop().get(function(value) {
        if (value != null) return value;
        for (var parent = this.parent(); parent; parent = parent.parent()) {
            if (typeof parent[from] === 'function') {
                value = parent[from]();
                if (value != null) return value;
            }
        }
        return onDefault ? onDefault.call(this) : defaultValue;
    });
    prop.onDefault = function(fn) {
        onDefault = fn;
        return this;
    };
    return prop;
};

/**
 * Creates a function that can be used as an event trigger or to bind an event
 * handler.
 *
 *    var click = c3.event();
 *    click(handler); // bind an event handler
 *    click();        // trigger the event
 *
 * @param {function} [defaultHandler] - a handler that is run after other handlers,
 *    defining the default behaviour for the event.
 */
c3.event = function(defaultHandler) {
    var handlers = [];

    function bind(handler, context) {
        handlers.push({
            handler: handler,
            context: context
        });
    }

    function emit(eventData) {
        var emitContext = this;
        eventData = eventData || {};

        _.each(handlers, function(handlerEntry) {
            handlerEntry.handler.call(handlerEntry.context || emitContext, eventData);
        });

        if (defaultHandler) defaultHandler.call(this, eventData);
    }

    function event() {
        if (typeof arguments[0] === 'function') {
            bind.apply(this, arguments);
        } else {
            emit.apply(this, arguments);
        }
        return this;
    }

    event.off = function(handler, context) {
        handlers = _.reject(handlers, function(handlerEntry) {
            var matchesHandler = handler == null || handlerEntry.handler == handler;
            var matchesContext = context == null || handlerEntry.context == context;
            return matchesHandler && matchesContext;
        });
        return this;
    };

    return event;
};
