/* global _, clusterVectors, figue */
"use strict";

function ControlChartDataModel (data) {

    var bounds = getMinMaxes(data.total.values),
        dataObj = null;

    function getMinMaxes() {
        var series = data.total.values,
            result = {
                minX: Number.MAX_VALUE,
                minY: Number.MAX_VALUE,
                maxX: Number.MIN_VALUE,
                maxY: Number.MIN_VALUE
            };
            
        _.each(series, function (element) {

            var x = element[0],
                y = element[1];

            if (x < result.minX) {
                result.minX = x;
            } else if (x > result.maxX) {
                result.maxX = x;
            }

            if (y < result.minY) {
                result.minY = y;
            } else if (y > result.maxY) {
                result.maxY = y;
            }
        });
        
        return {
            x: [result.minX, result.maxX],
            y: [result.minY, result.maxY]
        }
    }

    function getClusteredIssuePlotSeries(clusterThreshold, xScale, yScale) {
        var vectors = data.total.values,
            labels = [],
            clusteredVectors;

        _.each(vectors, function (vector, i) {
            labels.push(i);
        });

        clusteredVectors = clusterVectors(vectors, labels, clusterThreshold, xScale, yScale);

        return {
            clustered: _.filter(clusteredVectors, function (vector) { return vector.cluster; }),
            unclustered: _.reject(clusteredVectors, function (vector) { return vector.cluster; })
        };
    }


    dataObj = {
        rawData: data,
        values: data.total.values,
        bounds: bounds,
        aggregateMeanSeries: data.total.aggregatedMean,
        standardMeanSeries: data.total.mean,
        rawIssueSeries: data.total.values,
        getClusteredIssueSeries: getClusteredIssuePlotSeries
    }

    return dataObj;
}