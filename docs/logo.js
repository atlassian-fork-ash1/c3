var logo = function() {
    var gridDomain = [-0.1, 4.1];

    var left = [
        [ 2, 4 ],
        [ 0, 4 ],
        [ 0, 0 ],
        [ 2, 0 ]
    ];
    var right = [
        [ 2, 4 ],
        [ 4, 4 ],
        [ 4, 2 ],
        [ 2, 2 ],
        [ 4, 2 ],
        [ 4, 0 ],
        [ 2, 0 ]
    ];
    var logoLeft = c3.linePlot()
        .elementClass('logo-left')
        .data(left)
        .update(function(event) {
            var scale = this.xScale();
            event.selection.attr('stroke-width', scale(0.1));
        });
    var logoRight = c3.linePlot()
        .elementClass('logo-right')
        .data(right)
        .update(function(event) {
            var scale = this.xScale();
            event.selection.attr('stroke-width', scale(0.1));
        });
    return c3.component('c3-logo')
        .extend(c3.layerable())
        .addLayer('logo-left', logoLeft)
        .addLayer('logo-right', logoRight)
        .xDomain(gridDomain)
        .yDomain(gridDomain);
};