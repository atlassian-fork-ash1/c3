var composing = function() {
    var mousePosX = .5;
    document.addEventListener('mousemove', _.throttle(function(e) {
        var selection = component.selection();
        if (selection) {
            var selectionWidth = selection.node().parentNode.clientWidth;
            mousePosX = (e.pageX - (window.innerWidth - selectionWidth) / 2) / selectionWidth;
            component();
        }
    }, 100));

    var data = [
        1,
        2,
        4,
        3,
        6,
        5,
        8,
        9,
        11,
        10,
        12,
        8,
        9,
        7,
        6,
        3,
        1,
        2,
        1
    ];
    var fillScale = d3.scale.linear()
        .domain([0, .1, .3])
        .range(['hsl(220, 39%, 78%)', 'hsl(195, 39%, 78%)', '#eaeaea'])
        .clamp(true);

    var component = c3.component('composing')
        .extend(c3.plottable())
        .extend(c3.drawable())
        .elementTag('rect')
        .data(data)
        .xAccessor(function(d, i) {
            return i;
        })
        .yAccessor(function(d) {
            return d;
        })
        .yDomain([0, d3.max(data)])
        .update(function(event) {
            var xScale = this.xScale();
            var x = this.x();
            var y = this.y();
            var height = this.height();
            var halfSpace = x(null, 1) / 2;
            var containerNode = this.selection().node();
            event.selection
                .attr('height', function(d) {
                    return height - y(d);
                })
                .attr('width', halfSpace)
                .attr('x', function(d, i) {
                    return x(d, i) + halfSpace / 2;
                })
                .attr('y', y)
                .attr('fill', function(d, i) {
                    var distanceFromMouse = Math.abs(i / (data.length - 1) - mousePosX);
                    return fillScale(distanceFromMouse);
                });
        });

    return component;
};